const { Router } = require("express");
const {
  getAllLabs,
  getLab,
  createLab,
  deleteLab,
  updateLab,
} = require("../controllers/lab.controller");

const router = Router();

router.get("/lab", getAllLabs);
router.get("/lab/:id_laboratorio", getLab);
router.post("/lab", createLab);
router.delete("/lab/:id_laboratorio", deleteLab);
router.put("/lab/:id_laboratorio", updateLab);

module.exports = router;
